# -*- coding: utf-8 -*-
"""
Plantilla 1 de la práctica 2 s

Referencia: 
    https://scikit-learn.org/stable/modules/clustering.html
    https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    https://docs.scipy.org/doc/scipy/reference/spatial.distance.html
"""

import numpy as np

from sklearn.cluster import KMeans
from sklearn import metrics
import matplotlib.pyplot as plt
#from scipy.spatial import ConvexHull, convex_hull_plot_2d <--- esto lo usaremos para evaluar si usar kmeans. kmeans funciona solo si los clusters tienen forma convexa
#from scipy.spatial import Voronoi, voronoi_plot_2d <---- voronoi es util para ver la asignacion secuencial de los puntos a los clusters


# #############################################################################
# Aquí tenemos definido el sistema X de 1500 elementos (personas) con dos estados
<<<<<<< HEAD
archivo1 = "F:\DEV\GeoComp\practica2geocomp/Personas_de_villa_laminera.txt"
archivo2 = "F:\DEV\GeoComp\practica2geocomp/Franjas_de_edad.txt"
=======
archivo1 = "C:\DEV\GeoComp\practica2geocomp/Personas_de_villa_laminera.txt"
archivo2 = "C:\DEV\GeoComp\practica2geocomp/Franjas_de_edad.txt"
>>>>>>> 43ab7bd3cea8275ca60a3d6db43e59025e9174c9
X = np.loadtxt(archivo1,skiprows=1)
Y = np.loadtxt(archivo2,skiprows=1)
labels_true = Y[:,0]

header = open(archivo1).readline()
print(header)
print(X)
#Si quisieramos estandarizar los valores del sistema, haríamos:
#from sklearn.preprocessing import StandardScaler
#X = StandardScaler().fit_transform(X)  

plt.plot(X[:,0],X[:,1],'ro', markersize=1)
plt.show()

# #############################################################################
# Los clasificamos mediante el algoritmo KMeans
n_clusters=2

#Usamos la inicialización aleatoria "random_state=0"
kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(X)
labels = kmeans.labels_
silhouette = metrics.silhouette_score(X, labels) # <--- esto es el silhouette score (que no silhouette coefficient de acuerdo a wikipedia)

# Etiqueta de cada elemento (punto)
print(kmeans.labels_)
# Índice de los centros de vencindades o regiones de Voronoi para cada elemento (punto) 
print(kmeans.cluster_centers_)
#Coeficiente de Silhouette
print("Silhouette Coefficient: %0.3f" % silhouette)


# #############################################################################
# Predicción de elementos para pertenecer a una clase:
problem = np.array([[-1.5, -1], [1.5, -1]])
clases_pred = kmeans.predict(problem)
print(clases_pred)

# #############################################################################
# Representamos el resultado con un plot

unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]

plt.figure(figsize=(8,4)) ## <-- esto es el tamaño de plot
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=5) #<--- estos son los puntos de los clusters pintados de colores. markeredgecolor cambia el color del borde del punto

plt.plot(problem[:,0],problem[:,1],'o', markersize=12, markerfacecolor="red")

plt.title('Fixed number of KMeans clusters: %d' % n_clusters)
plt.show()

'''
RAND INDEX
sean N puntos y dos clusterizaciones

se llama a al numero de pares de puntos que están en el mismo cluster con X y con Y
se llama b al numero de pares de puntos que están en diferentes clusters con X y con Y
R = 2(a+b)/n(n-1)
'''

import warnings
warnings.filterwarnings("ignore")

### calculamos el coef de silhouette para varios numeros de clusters
vector_silhouette = []
vector_num_clusters = list(range(2,16))
for k in range(2,16):
    kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
    labels = kmeans.labels_
    silhouette = metrics.silhouette_score(X, labels) 
    
    vector_silhouette.append(silhouette)
    


plt.plot(vector_num_clusters,vector_silhouette, markersize=12, markerfacecolor="red")
plt.title("Análisis de K-means")
plt.xlabel("Número de clusters")
plt.ylabel("Coeficiente de Silhouette")
plt.show()


'''
plt.plot(X, y, color='r', label='sin') 
plt.plot(X, z, color='g', label='cos')
'''
# La gráfica nos dice que el número óptimo de clusters es 3. 
#No obstante, nos conviene estudiar la convexidad de los datos para ver si K-means es fiable

### Diagrama de Voronoi

#### usamos kmeans predict para ver un idividuo dea valores a = (1/2, 0) b = (0,-3)

kmeans.predict(X)


