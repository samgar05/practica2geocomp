# -*- coding: utf-8 -*-
"""
Plantilla 2 de la práctica 2

Referencias:
    https://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html
    https://towardsdatascience.com/dbscan-d690cbae4c5d
    https://www.aaai.org/Papers/KDD/1996/KDD96-037.pdf
"""

import numpy as np

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets import make_blobs

import matplotlib.pyplot as plt

# #############################################################################
# Aquí tenemos definido el sistema X de 1500 elementos (personas) con dos estados
archivo1 = "C:\DEV\GeoComp\practica2geocomp/Personas_de_villa_laminera.txt"
archivo2 = "C:\DEV\GeoComp\practica2geocomp/Franjas_de_edad.txt"
X = np.loadtxt(archivo1,skiprows=1) #eje X guardado en array con numpy
Y = np.loadtxt(archivo2,skiprows=1) #eje Y guardado en array con numpy
labels_true = Y[:,0] # los lables, dulces y estres

header = open(archivo1).readline()
print(header)
print(X)
#Si quisieramos estandarizar los valores del sistema, haríamos:
#from sklearn.preprocessing import StandardScaler
#X = StandardScaler().fit_transform(X)  

plt.plot(X[:,0],X[:,1],'ro', markersize=1) # plot de los datos
plt.show()
 
# #############################################################################
# Los clasificamos mediante el algoritmo DBSCAN
# Existen dos parametros con DBSCAN, el tamaño de las bolitas que hacemos ---epsilon--- y la cantidad de puntos que aceptamos para considerar a un punto Core Point
# el parametro DBSCAN ---min_samples---

# La métrica también la elegimos porque dependiendo de una u otra tenemos variabilidad

epsilon = 0.4 ####### <- tamaño de los blobs pequeñitos que metemos 

db = DBSCAN(eps=epsilon, min_samples=10, metric='euclidean').fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_ #extraemos el atributo labels del objeto de DBSCAN

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0) 
n_noise_ = list(labels).count(-1) #numero de outliers

print('Estimated number of clusters: %d' % n_clusters_)
print('Estimated number of noise points: %d' % n_noise_)
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))  #adjusted rand score es parecido a Silhoutte Coefficient en que cuanto más cerca de 1 mejor y cuanto mas cerca de -1 peor. es sensible al chance
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels))


# #############################################################################
# Representamos el resultado con un plot

unique_labels = set(labels) # quitamos lables repetidas con el datatype set
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))] #lista de colores

plt.figure(figsize=(8,4))
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=5)

    xy = X[class_member_mask & ~core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=3)

plt.title('Estimated number of DBSCAN clusters: %d' % n_clusters_)
plt.show()

'''
SILHOUTTE Coeff
Una vez generados nuestros clusters, obtenemos una métrica de la bondad del clustering
Para cada punto, lo indexamos con i, tomamos a(i) la distancia media entre el punto i y el resto de su cluster
a(i) es una medida (cuanto menor mejor) de cómo de bien está ese punto en su cluster (cómo en casa se siente)

ahora para cada punto i, tomamos b(i) la distancia media mínima entre i y El resto de clusters que no sea el suyo
esto mide cómo de "perdido" está el punto i. El cluster al que pertenezca el punto de distancia b(i) se le llama "cluster vecino"

llamamos el VALOR DE SILHOUETTE de i a 
s(i) = (b(i) - a(i))/max{b(i),a(i)}
(si el cluster solo contuviera a i, s(i) es 0)

Al valor medio de todos los valores de silhouette se le llama COEFFICIENTE DE SILHOUETTE (wikipedia da otra definicion un poco diferente)

'''

### estudio del epsilon
### calculamos el coef de silhouette para varios epsilon
from numpy import linspace

vector_silhouette_euclidean = []
vector_silhouette_manhattan = []
vector_epsilon = list(linspace(0.1,0.4,num = 10))

for epsilon in vector_epsilon:
    db = DBSCAN(eps=epsilon, min_samples=10, metric='euclidean').fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_ #extraemos el atributo labels del objeto de DBSCAN
    
    silhouette = metrics.silhouette_score(X, labels)
    vector_silhouette_euclidean.append(silhouette)
    
for epsilon in vector_epsilon:
    db = DBSCAN(eps=epsilon, min_samples=10, metric='manhattan').fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_ #extraemos el atributo labels del objeto de DBSCAN
    
    silhouette = metrics.silhouette_score(X, labels)
    vector_silhouette_manhattan.append(silhouette)
    
plt.plot(vector_epsilon,vector_silhouette_euclidean, color='blue', label = 'euclidean')
plt.plot(vector_epsilon,vector_silhouette_manhattan, color='red', label = 'manhattan')
plt.legend()
plt.title("Análisis de DBSCAN")
plt.xlabel("Valor del parámetro epsilon")
plt.ylabel("Coeficiente de Silhouette")
plt.show()





