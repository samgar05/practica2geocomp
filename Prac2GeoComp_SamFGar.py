'''
Samuel Fernando Garcia Herrera 
Prac2. Diagrama de Voronoi, analisis de datos y uso de KMEANS y DBSCAN
'''
#Estudiamos Clustering con un conjunto de datos y dos algoritmos de clustering. 

from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn import metrics

from scipy.spatial import ConvexHull
from scipy.spatial import Voronoi, voronoi_plot_2d

import matplotlib.pyplot as plt
import numpy as np

#Hacemos un plot de los datos
archivo1 = "C:\DEV\GeoComp\practica2geocomp/Personas_de_villa_laminera.txt"
archivo2 = "C:\DEV\GeoComp\practica2geocomp/Franjas_de_edad.txt"
X = np.loadtxt(archivo1,skiprows=1) #eje X guardado en array con numpy
Y = np.loadtxt(archivo2,skiprows=1) #eje Y guardado en array con numpy
labels_true = Y[:,0] # los lables, dulces y estres

plt.plot(X[:,0],X[:,1],'ro', markersize=1) # plot de los datos
plt.title("Personas de Villa Laminera")
plt.xlabel("Nivel de Estres")
plt.ylabel("Gusto por los Dulces")
plt.show()

###### APLICACION DE KMEANS ######
# Los clasificamos mediante el algoritmo KMeans
n_clusters=3

#Usamos la inicializacion aleatoria "random_state=0"
kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(X)
labels = kmeans.labels_
silhouette = metrics.silhouette_score(X, labels) # <--- esto es el silhouette score (que no silhouette coefficient de acuerdo a wikipedia)

#Coeficiente de Silhouette
print("Silhouette Coefficient: %0.3f" % silhouette)

# Representamos el resultado de KMEANS en un plot
unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]

plt.figure(figsize=(8,4)) ## <-- esto es el tamaño de plot
for k, col in zip(unique_labels, colors):
    if k == -1:
        # usamos un color negro para el ruido.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=5) #<--- estos son los puntos de los clusters pintados de colores
    
    # Calcula y dibuja el recubrimiento convexo para cada cluster para ver si corresponde con la seleccion de clusters
    hull = ConvexHull(xy)
    for simplex in hull.simplices:
        plt.plot(xy[simplex, 0], xy[simplex, 1], 'k-')

plt.title('Numero de clusters por K-Means: %d' % n_clusters)

#### usamos kmeans predict para ver un idividuo de valores a = (1/2, 0) b = (0,-3)
puntos_a_predecir = np.array([[0.5, 0], [0, -3]])
clases_prediccion = kmeans.predict(puntos_a_predecir)
plt.plot(puntos_a_predecir[:,0],puntos_a_predecir[:,1],'o', markersize=12, markerfacecolor="green")
plt.show()

print("El punto A (0.5, 0) tiene la etiqueta: {}".format(clases_prediccion[0]))
print("El punto B (0, -3) tiene la etiqueta: {}".format(clases_prediccion[1]))

#Hacemos un diagrama de voronoi
#encontramos sus centroides y los ponemos junto al diagrama
fig, ax = plt.subplots()

centroides = kmeans.cluster_centers_
vor = Voronoi(centroides)
figura_puntos = plt.plot(X[:,0],X[:,1],'ko', markersize=1) 
figura_voronoi = voronoi_plot_2d(vor, ax = ax, show_vertices=False, show_points=True, line_colors='black', line_width=1, point_size = 20)

plt.xlim(-3.5, 3.5)
plt.ylim(-3.5, 3.5)
plt.title("Diagrama de Voronoi de los datos")
plt.show()


###### ESTUDIO DEL NUMERO DE CLUSTERS CON KMEANS ######
### calculamos el coef de silhouette para varios numeros de clusters
vector_silhouette = []
vector_num_clusters = list(range(2,16))
for k in range(2,16):
    kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
    labels = kmeans.labels_
    silhouette = metrics.silhouette_score(X, labels) 
    
    vector_silhouette.append(silhouette)

plt.plot(vector_num_clusters,vector_silhouette)
plt.title("Analisis de K-means")
plt.xlabel("Numero de clusters")
plt.ylabel("Coeficiente de Silhouette")
plt.show()

#mejor numero de clusters 3


###### APLICACION DE DBSCAN ######
# Existen dos parametros con DBSCAN, el tamaño de las bolitas que hacemos ---epsilon--- y la cantidad de puntos que aceptamos para considerar a un punto Core Point
# el parametro DBSCAN ---min_samples---
epsilon = 0.323 ####### <- ponemos el epsilon que hemos calculado 

db = DBSCAN(eps=epsilon, min_samples=10, metric='euclidean').fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_ 

n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0) 
n_noise_ = list(labels).count(-1) #numero de outliers

print('Numero de clusters: %d' % n_clusters_)
print('Numero de outliers %d' % n_noise_)
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels))

# Representamos el resultado de DBSCAN en un plot
unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]

plt.figure(figsize=(8,4)) ## <-- esto es el tamaño de plot
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=5) #<--- estos son los puntos de los clusters pintados de colores
    
plt.title('Numero de clusters por DBSCAN: %d' % n_clusters)
plt.show()



###### ESTUDIO DEL EPSILON ######
### calculamos el coef de silhouette para varios epsilon y con dos metricas distintas
from numpy import linspace

vector_silhouette_euclidean = []
vector_silhouette_manhattan = []
vector_epsilon = list(linspace(0.1,0.4,num = 100))

for epsilon in vector_epsilon:
    db = DBSCAN(eps=epsilon, min_samples=10, metric='euclidean').fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_ #extraemos el atributo labels del objeto de DBSCAN
    
    silhouette = metrics.silhouette_score(X, labels)
    vector_silhouette_euclidean.append(silhouette)
    
for epsilon in vector_epsilon:
    db = DBSCAN(eps=epsilon, min_samples=10, metric='manhattan').fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_ #extraemos el atributo labels del objeto de DBSCAN
    
    silhouette = metrics.silhouette_score(X, labels)
    vector_silhouette_manhattan.append(silhouette)
    
plt.plot(vector_epsilon,vector_silhouette_euclidean, color='blue', label = 'euclidean')
plt.plot(vector_epsilon,vector_silhouette_manhattan, color='red', label = 'manhattan')
plt.legend()
plt.title("Analisis de DBSCAN")
plt.xlabel("Valor del parametro epsilon")
plt.ylabel("Coeficiente de Silhouette")
plt.show()

### Obtenemos nuestro mejor epsilon
pares_euc = dict(zip(vector_silhouette_euclidean, vector_epsilon))
pares_man = dict(zip(vector_silhouette_manhattan, vector_epsilon))
mejor_silhouette_euc = max(list(pares_euc.keys()))
mejor_silhouette_man = max(list(pares_man.keys()))
mejor_epsilon_euclidean = pares_euc[mejor_silhouette_euc]
mejor_epsilon_manhattan = pares_man[mejor_silhouette_man]

print('El mejor epsilon para la metrica euclidea: {}'.format(mejor_epsilon_euclidean))
print('El mejor epsilon para la metrica de manhattan: {}'.format(mejor_epsilon_manhattan))
